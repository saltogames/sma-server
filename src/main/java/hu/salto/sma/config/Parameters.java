package hu.salto.sma.config;

public class Parameters {

    // world
    public static final int BLOCK_SIZE = 23;
    
    public static final int WORLD_W = 1800;
    public static final int WORLD_H = 1800;
    public static final int LADDER_DENSITY = 1;             // One is like one in a 1000x1000 area. One means very rare.
    public static final int INIT_NPC_COUNT = 30;            
    
    public static final int PUNCH_DISTANCE = 22;
    public static final int KICK_DISTANCE = 30;
    public static final int PUNCH_DAMAGE = 2;
    public static final int KICK_DAMAGE = 3;        
    public static final int JUMP_TIME = 9;               // tick
    
    
    public static final int FALL_SPEED = 11;
    public static final float NPC_WALKING_SPEED = 5.5f;          // px / tick
    public static final float PLAYER_WALKING_SPEED = 6.2f;       // px / tick    
    public static final float PLAYER_JUMP_SPEED = 6.2f;         // px / tick
    
    public static final double DAMAGED_KNOCKBACK = 3;           // px / frame (tick)
    // public static final double PUNCH_KNOCKBACK = 1;          // px / frame (tick)
    // public static final double KICK_KNOCKBACK = 2.5;         // px / frame (tick)
    
    public static final double NPC_SIGHT_RANGE_X = 750;           // px
    public static final double NPC_SIGHT_RANGE_Y = 100;           // px
    
    // engine
    public static final long SCHEDULER_RATE = 55;           // ms       
}
