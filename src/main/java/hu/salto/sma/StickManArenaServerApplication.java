package hu.salto.sma;

import hu.salto.sma.engine.Game;
import hu.salto.sma.engine.World;
import hu.salto.sma.engine.controllers.UserActionController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class StickManArenaServerApplication {
	
	@Autowired
	public static UserActionController uac;
	
        public static World w;
        
	public static void main(String[] args) {            
            Game.createRandomWorld();            
            SpringApplication.run(StickManArenaServerApplication.class, args);
            /*Game.startNewRandomWorld();
            SpringApplication.run(SpaceOdysseyServerApplication.class, args);*/
	}
}
