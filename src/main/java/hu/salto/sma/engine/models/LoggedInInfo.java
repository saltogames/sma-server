/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.salto.sma.engine.models;

import hu.salto.sma.engine.Player;

/**
 *
 * @author gszemes
 */
public class LoggedInInfo {
    private Player player;
    private int[][] map;
    
    public LoggedInInfo(Player player, int[][] map) {
        this.player = player;
        this.map = map;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public int[][] getMap() {
        return map;
    }

    public void setMap(int[][] map) {
        this.map = map;
    }
    
    
}
