package hu.salto.sma.engine;
import hu.salto.sma.config.Parameters;
import java.util.HashSet;
import java.util.Set;
import sun.text.normalizer.UCharacter;

public class Player extends Character{        
    public Set<Integer> currentlyPressedKeys = new HashSet<Integer>();    
    public Player(){
        super();
    }
    public Player(double id, double x, double y, String color) {
        super(id, x, y, color);
        setMaxLife(100);
        setLife(100);
    }	

    public void step(int[][] map, int blockSize){        
        super.step(map, blockSize);
        if (getState() == Status.DIED) return;
        
        // if out of map, skip        
        int mapX = (int)Math.floor(getX()/blockSize) + 1;
        int mapY = (int)Math.floor(getY()/blockSize) + 2;                
        try {
            int type = map[mapX][mapY];
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }                     
        
        if (getStateSustension() > 0) {
            // handle sustained states, but nothing else
            int jumpCount = Parameters.JUMP_TIME - getStateSustension();               
            int scale = 2;
            double offsetY = Math.abs(Parameters.JUMP_TIME/2-jumpCount)*scale-Parameters.JUMP_TIME/2*scale;
            switch(getState()){
                case JUMPRIGHT :                 
                    setX(getX()+Parameters.PLAYER_JUMP_SPEED);
                    setOffsetY(offsetY);
                    break;
                case JUMPLEFT :             
                    setX(getX()-Parameters.PLAYER_JUMP_SPEED);
                    setOffsetY(offsetY);
                    break;
                case JUMP :                 
                    setOffsetY(offsetY);
                    break;
            }            
            return;
        }   
        
        setOffsetX(0);
        setOffsetY(0);
        
        // move
        Status s = getState();
        if (currentlyPressedKeys.size() > 0) {                 
            for (int kc : currentlyPressedKeys){   
                if (getStateSustension() > 0) continue;
                switch (kc) {
                    case 81 : // Q : PUNCH                       
                        if (s != Status.FALLING){
                            if (this.direction == Direction.RIGHT){
                                setState(Status.PUNCHINGLEFT);
                            } else {
                                setState(Status.PUNCHINGRIGHT);
                            }                           
                            setStateSustension(3);
                        }
                        break;  
                    case 87 : // Q : KICK                      
                        if (s != Status.FALLING){
                            if (this.direction == Direction.RIGHT){
                                setState(Status.KICKINGLEFT);
                            } else{
                                setState(Status.KICKINGRIGHT);
                            }                           
                            setStateSustension(3);
                        }
                        break;   
                    case 40 : // down                        
                        if (s == Status.CLIMBING || s == Status.STANDING){
                            if (map[mapX][mapY] == 2 || map[mapX][mapY+1] == 2){
                                // there is a ladder
                                setState(Status.CLIMBING);
                                direction = Direction.DOWN;
                                setY(getY()+5);
                            }                             
                        }                        
                        break;
                    case 38 : // up                        
                        if (s == Status.WALKING || s == Status.STANDING || s == Status.CLIMBING){
                            if (map[mapX][mapY] == 2){
                                // there is a ladder
                                setState(Status.CLIMBING);
                                direction = Direction.UP;
                                setY(getY()-5);                                
                            } else {
                                 setStateSustension(Parameters.JUMP_TIME);                                
                                 switch (direction) {
                                    case LEFT : 
                                        setState(Status.JUMPRIGHT);                                        
                                        break;  
                                    case RIGHT : 
                                        setState(Status.JUMPLEFT);
                                        break;  
                                    case NONE : 
                                        setState(Status.JUMP);
                                        break;
                                    default:
                                        break;
                                 }                                 
                            }          
                        }                                                
                        break;
                    case 39 : // <-                      
                        if (getState() != Status.FALLING){
                            setX(getX()+Parameters.PLAYER_WALKING_SPEED);
                            setState(Status.WALKING);
                            direction = Direction.LEFT;
                        }                            
                        break;
                    case 37 : // ->
                        if (getState() != Status.FALLING){
                            setX(getX()-Parameters.PLAYER_WALKING_SPEED);
                            setState(Status.WALKING);
                            direction = Direction.RIGHT;                                        
                        }
                        break;       
                }
            }
        } else {
            if (s != Status.FALLING) {
                setState(Status.STANDING);
                direction = Direction.NONE;
            }
        }
        
        
    };
    
    protected void die() {
        super.die();        
        System.out.println("Player died.");
    }
    
    
    @Override
    public String toString() {
        return super.toString();
    }
}
