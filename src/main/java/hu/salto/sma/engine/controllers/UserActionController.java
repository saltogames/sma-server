package hu.salto.sma.engine.controllers;

import hu.salto.sma.engine.Game;
import hu.salto.sma.engine.models.UserKeyStrokeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import hu.salto.sma.engine.Player;

@Controller
public class UserActionController {

    @Autowired
    private SimpMessagingTemplate template;

    @MessageMapping("/keydown")
    public void pushUserAction(UserKeyStrokeMessage message){
        Player p = Game.world.getPlayer(message.uid);
        if (p == null) return;
        p.currentlyPressedKeys.add(message.keyCode);
    }
    @MessageMapping("/keyup")
    public void pushUserActionStop(UserKeyStrokeMessage message){
        Player p = Game.world.getPlayer(message.uid);
        if (p == null) return;
        p.currentlyPressedKeys.remove(message.keyCode);
    }
}
