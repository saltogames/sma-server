package hu.salto.sma.engine.controllers;

import hu.salto.sma.config.Parameters;
import hu.salto.sma.engine.Game;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

@EnableScheduling
@Controller
public class ScheduledController {
	
    private static int stepCount = 0;

    @Autowired
    private SimpMessagingTemplate template;

    @Scheduled(fixedRate = Parameters.SCHEDULER_RATE)
    @SendTo("/broadcast/characters")
    public void broadcastCharacters() {
            stepCount++;
            if (stepCount%500 == 0) System.out.println("step:" + stepCount);            
            Game.world.step();
            // broadcast all status
            this.template.convertAndSend("/broadcast/characters", Game.world.getCharactersArray());
    }
}
