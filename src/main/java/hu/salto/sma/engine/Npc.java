package hu.salto.sma.engine;

import hu.salto.sma.config.Parameters;

public class Npc extends Character{    
    public Npc(){
        super();
    }
    public Npc(int id, double x, double y, String color) {
        super(id, x, y, color);        
        setState(Status.FALLING);
        setMaxLife(50);
        setLife(50);
    }	

    
    @Override
    public String toString() {
        return super.toString();
    }        

    @Override
    public void step(int[][] map, int blockSize) {
        super.step(map, blockSize);
        if (getState() == Status.DIED) return;
        
        if (getStateSustension() > 0) {
            return;
        }   
        
        
        // if out of map, skip
        int mapX = (int)Math.floor(getX()/blockSize) + 1;
        int mapY = (int)Math.floor(getY()/blockSize) + 2;       
        try {
            int type = map[mapX][mapY];
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }
        
        // ai
        String intention = "none";
        Player target = null;
        for (Object o : Game.world.getPlayersArray()){
            Player p = (Player) o;            
            if (
                Math.abs(p.getX() - getX()) < Parameters.NPC_SIGHT_RANGE_X &&
                Math.abs(p.getY() - getY()) < Parameters.NPC_SIGHT_RANGE_Y
            ) {
                target = p;
                intention = "engage";
                if (
                    Math.abs(p.getX() - getX()) < Parameters.PUNCH_DISTANCE                    
                ) {                    
                    intention = "fight";
                }
                
            }
        }
        if (intention.equals("engage")){
            if (target.getX() > getX()) {
                direction = Direction.RIGHT;
            } else {
                direction = Direction.LEFT;
            }            
        } else if (intention.equals("fight")){
            double r = Math.random();
            if (r<0.1){
                if (direction == Direction.RIGHT) setState(Status.KICKINGRIGHT);
                if (direction == Direction.LEFT) setState(Status.KICKINGLEFT);
                setStateSustension(3);
            } else if (r<0.2) {
                if (direction == Direction.RIGHT) setState(Status.PUNCHINGRIGHT);
                if (direction == Direction.LEFT) setState(Status.PUNCHINGLEFT);
                setStateSustension(3);
            } else {                        
                setState(Status.STANDING);
            }
        }
        switch (getState()) {            
            case FALLING :
                break;
            case WALKING :                                
                if (direction == Direction.NONE) {
                    if (Math.random() < 0.5) direction = Direction.LEFT;
                    else direction = Direction.RIGHT;
                }
                // if ladder, climb up
                if (map[mapX][mapY] == 2 && intention.equals("none")){
                    setState(Status.CLIMBING);
                    direction = Direction.UP;
                }
                // turn back upon edges
                if (direction == Direction.LEFT){
                    try {
                        int blockLeft = map[mapX-1][mapY];
                        if (blockLeft == 1 || blockLeft >= 110 ){
                            direction = Direction.RIGHT;
                        } 
                    } catch (ArrayIndexOutOfBoundsException e){
                        direction = Direction.RIGHT;
                    }
                                       
                }                
                if (direction == Direction.RIGHT){
                    try {
                        int blockRight = map[mapX+1][mapY];
                        if (blockRight == 1 || blockRight >= 110){
                            direction = direction.LEFT;                
                        } 
                    } catch (ArrayIndexOutOfBoundsException e){
                        direction = Direction.LEFT;
                    }
                }                
                // may take a rest
                if (Math.random() < 0.01) {
                    setState(Status.STANDING);
                    direction = Direction.NONE;
                }
                break;   
            case STANDING :                   
                // may start to walk
                if (Math.random() < 0.05) setState(Status.WALKING);
                break;
            case CLIMBING :                 
                break;          
            default:
                break;
        }
        
        // update coordinates regarding to statuses
        switch (getState()) {
            case FALLING :
                break;
            case WALKING :
                if (direction == Direction.LEFT) setX(getX()-Parameters.NPC_WALKING_SPEED);
                if (direction == Direction.RIGHT) setX(getX()+Parameters.NPC_WALKING_SPEED);
                break;
            case STANDING :   
                // zzz...                
                break;
            case CLIMBING :
                setX(mapX*blockSize-8);
                if (direction == Direction.UP) setY(getY() - 5);
                if (direction == Direction.DOWN) setY(getY() + 6);
                break;
            default:
                break;
        }                 
    }
}
