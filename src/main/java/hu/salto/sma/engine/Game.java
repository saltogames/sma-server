package hu.salto.sma.engine;
import hu.salto.sma.config.Parameters;

public class Game {
    public static World world;
    public static void createRandomWorld() {
        System.out.println("<GAME> Starting new random world.");
        System.out.println("<GAME> Width: " + Parameters.WORLD_W + "  Height: " + Parameters.WORLD_H);
        System.out.println("<GAME> NPC count: " + Parameters.INIT_NPC_COUNT);
        world = new World(
            Parameters.WORLD_W,
            Parameters.WORLD_H,
            Parameters.BLOCK_SIZE,
            Parameters.INIT_NPC_COUNT
        );
    }    
}
